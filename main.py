from vision import sendRequest
import os
import json
from app import app
from flask import flash, request, redirect, render_template, session
from werkzeug.utils import secure_filename

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use a service account
cred = credentials.Certificate('image-vision-ml-key.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

# Set allowed extension
allowedExtensions = set(['png', 'jpg', 'jpeg', 'gif'])


def allowedFile(filename):
    return '.'in filename and filename.rsplit('.', 1)[1].lower() in allowedExtensions


@app.route('/')
def upload():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        docs = db.collection(u'history').get()
        for doc in docs:
            description = json.loads(json.dumps(doc.to_dict()))['description']
            flash(description)
        return render_template('upload.html')


@app.route('/login', methods=['POST'])
def login():
    docs = db.collection(u'users').get()
    for doc in docs:
        username = json.loads(json.dumps(doc.to_dict()))['username']
        password = json.loads(json.dumps(doc.to_dict()))['password']
        if request.form['password'] == password and request.form['username'] == username:
            session['logged_in'] = True
    return upload()


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return upload()


@app.route('/', methods=['POST'])
def uploadFile():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect(request.url)
        if file and allowedFile(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            result = sendRequest(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            description = result[0].description
            doc_ref = db.collection(u'history').document()
            doc_ref.set({
                u'description': description,
                u'filename': filename
            })
            return redirect('/')


if __name__ == "__main__":
    app.run(host="0.0.0.0", port="8080", debug=True)